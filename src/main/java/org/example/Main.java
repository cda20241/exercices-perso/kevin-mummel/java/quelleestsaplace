import java.text.Normalizer; // Normaliser du texte
import java.util.Scanner; // Lire l'entrée utilisateur

public class Main {

    public static void main(String[] args) {
        // Créer l'objet Scanner et l'appeler inputScanner
        // l'entrée utilisateur se fera a partir de la console (System.in)
        Scanner inputScanner = new Scanner(System.in);
        // Afficher la chaine de caractère entre guillemet
        System.out.print("Hello Man, Tape ta phrase stp : ");
        // Enregistrer dans la variable String sentence l'entrée utilisateur
        String sentence = inputScanner.nextLine();
        // Afficher la chaine de caractère entre guillemet
        System.out.print("Et maintenant, Tape un caractère stp : ");
        // Enregistrer dans la variable String character l'entrée utilisateur
        String character = inputScanner.nextLine();
        // Fermer l'entrée utilisateur pour libérer des ressources
        inputScanner.close();

        // On initialise la variable "sameCharacterCounter" à 0
        int sameCharacterCounter = 0;

        // Supprimer l'accent du premier caractère de la variable "character"
        // et l'enregistrer dans la variable "normalizedCharacter"
        char normalizedCharacter = removeAccents(character.charAt(0));

        // Créer une bloucle dans laquelle on parcour chaque position de la
        // variable "position" jusqu'à ce que le nombre de position soit inférieur
        // à la longeur de "sentence"
        for (int position = 0; position < sentence.length(); position++) {
            // On supprime les accents et enregistre le caractère dans la variable "currentChar"
            char currentChar = removeAccents(sentence.charAt(position));

            // Si le caractère "currentChar" est egale au caractère enregistré dans "normalizedCharacter"
            if (currentChar == normalizedCharacter) {
                // Alors on incrémente de 1 la variable "sameCharacterCounter"
                sameCharacterCounter++;

                // Afficher le caratère à sa position
                System.out.println("On retrouve le caractère " + character + " à la position " + position);
            }
        }
        // Si la variable "sameCharacterCounter" est vide
        if (sameCharacterCounter == 0) {
            // Alors on affiche que le caractère n'est pas présent
            System.out.println("Le caractère " + character + " n'est pas présent dans la phrase.");
        }
    }

    public static char removeAccents(char input) { // Création d'une méthode pour supprimer les accents
        // On créer une variable "normalized" dans laquelle on enregistre le input avec les accents séparées
        // des lettres selon la norme "Normalizer.Form.NFD"
        String normalized = Normalizer.normalize(String.valueOf(input), Normalizer.Form.NFD);
        // Remplace dans la variable "normalized" tout les accents (\p{InCombiningDiacriticalMarks}+) par du vide
        // et renvoie le premier caractere de la chaine normalisé
        return normalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "").charAt(0);
    }
}
